
# **************************************************************************************************
# EJECUCION DE LOS TPS
# **************************************************************************************************

# Ejecucion TPN1

1º Ir a la carpeta TPN1.
2º Abrir una terminar a esa altura.
3º Ejecutar.
	>_python main.py

# Ejecucion TPN2

1º Ir a la carpeta TPN2.
2º Abrir una terminar a esa altura.
3º Ejecutar.
	>_python main.py

# NOTA 1 : en ambos casos el input "cargado" es input.txt.
# Si se quiere ejecutar para el otro input (inputMasGrande.txt) debera reemplazar:
	* Para el TP1, linea 29 de main.py
		f = open('input.txt','r')
		por
		f = open('inputMasGrande.txt','r')
	* Para el TP2, linea 45 de main.py
		f = open('input.txt','r')
		por
		f = open('inputMasGrande.txt','r')

# NOTA 2 : Si se quiere una impresion de los pasos desarrollados descomentar las lineas borrando '#'
# Estas lineas se comentaron para que no influyera en el tiempo de procesamiento.
* Para el TP2:
	* En main.py lineas 18, 19, 21, 24, 89 y 102.
	* En ModeloDualPDC.py linea 46.
	* En ModeloAuxPDC.py linea 43.

# **************************************************************************************************
