
from ModeloPDC import *
from GeneradorPDC import *

from time import time

# ****************************************************************************
# ******************************* Sub - Metodos ******************************
# ****************************************************************************

def separador():
    print("*******************************************************************"+
    "*************************************************************************"+
    "******");

# ****************************************************************************
# ***************************** Metodo Principal *****************************
# ****************************************************************************

#Iniciamos el timer
start_time = time()

# Declaracion de inputs.
anchoRollo = 0;
cantidad = [];
ancho = [];

# Lectura de inputs.
f = open('input.txt','r')
message = f.read()                  ## Leemos el archivo
message = message.rsplit('\n')
contador=1;
for linea in message:
    if(contador==2):
        anchoRollo = int(linea[0:len(linea)]);
    if(contador>=4):
        indice_separator = linea.index(':');
        cant = linea[0:indice_separator];
        anch =  linea[indice_separator+1:len(linea)];
        cantidad.append(int(cant));
        ancho.append(int(anch));
    contador=contador+1;
f.close()

# Ordenamos los inputs (cantidad:ancho) en base a ancho.
tupla = list(zip(ancho, cantidad));
tuplaOrdenada = sorted(tupla, reverse=True);
ancho, cantidad = zip(*tuplaOrdenada)

# Generamos y obtenemos los patrones.
generador = GeneradorPDC(ancho, anchoRollo);
patrones = generador.generarPatrones();
# Resolvemos el problema a traves del modelo de programacion lineal.
ModeloPDC.resolverProblema(patrones, cantidad);

# Mostramos el tiempo de ejecucion.
separador();
execute_time = time() - start_time;
print("Tiempo de ejecucion transcurrido : %.10f segundos." % execute_time);

# ****************************************************************************
# ****************************************************************************