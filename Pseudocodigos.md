
# **************************************************************************************************
# Pseudocodigo TP1
# **************************************************************************************************

(1) Lectura de inputs y definición de constantes.
    anchoRollo = ...; (ancho del rollo sobre el que se harán los cortes)
    anchosRollitos = [...]; (ancho de cada rollito)
    cantidadRollitos = [...]; (demanda sobre cada rollito)
(2) Inicializamos variables (listaDePatrones=[]).
(3) Generamos los patrones maximales a través de algoritmo desarrollado en GeneradorPDC.py con anchoRollo, anchosRollitos y cantidadRollitos como inputs, y se lo entregamos a listaDePatrones.
(4) Entregamos la lista de patrones generada a ModeloPDC.py (que es igual a ModeloPrimalPDC.py del TP2) y el solver hace el procesamiento para encontrar la solución óptima.
(5) Mostramos las soluciones, el valor mínimo de rollos a usar y el tiempo de ejecución transcurrido.

# **************************************************************************************************
# Pseudocodigo TP2
# **************************************************************************************************

(1) Lectura de inputs y definición de constantes.
    anchoRollo = ...; (ancho del rollo sobre el que se harán los cortes)
    anchosRollitos = [...]; (ancho de cada rollito)
    cantidadRollitos = [...]; (demanda sobre cada rollito)
(2) Inicializamos variables.
    listaDePatrones=[]
    y=[]; (nivel de importancia de cada rollito)
    u=[]; (nuevo patrón generado)
(3) Resolvemos ModeloDualPDC.py con listaDePatrones y cantidadRollitos como inputs, y obtenemos y*.
(4) Resolvemos el ModeloAuxPDC.py con y*, anchosRollitos y anchoRollo como inputs, y obtenemos el nuevo patrón u*.
(5) Chequeamos: si sum(u*.y*)<=1 no es un patrón que mejora lo que tenemos, termina la iteración y hacemos (6), sino (sum(u*.y*)>1) agregamos u* a listaDePatrones y volvemos a (3).
(6) Resolvemos el ModeloPrimalPDC.py con listaDePatrones y cantidadRollitos, y obtenemos la solución óptima p* (lista de cantidad de veces usado el patrón i).
(7) Mostramos las soluciones (p*), el valor mínimo de rollos a usar y el tiempo de ejecución transcurrido.
