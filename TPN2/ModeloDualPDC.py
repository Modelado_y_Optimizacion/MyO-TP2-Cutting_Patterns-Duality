
# *********************************************************************************************************************************
# CONSIGNA
# Cuál es la mínima cantidad de rollos de 3 metros de ancho que necesitamos fabricar para satisfacer la siguiente demanda?
# 25 rollos de 250 cm de ancho.
# 97 rollos de 135 cm de ancho.
# 610 rollos de 108 cm de ancho.
# 395 rollos de 93 cm de ancho.
# 211 rollos de 42 cm de ancho.
# *********************************************************************************************************************************

from pyscipopt import Model, quicksum

class ModeloDualPDC:
    
    @staticmethod
    def resolverDual(patrones, demandaBarras):
        
        # Modelo
        model = Model("TP2 : Patrones de corte - Dual")  

        # Input
        cantidadDeY = range(len(demandaBarras));

        # Variables
        y = [model.addVar(vtype="C") for j in cantidadDeY]

        # Funcion objetivo
        model.setObjective(quicksum(nBarra[1] * y[nBarra[0]] for nBarra in demandaBarras), sense="maximize");

        # Restricciones
        cont=1;
        for patron in patrones:
            model.addCons(quicksum(patr[1] * y[patr[0]] for patr in patron) <= 1, "Rest. Nº "+str(cont))
            cont=cont+1;

        # Optimizacion
        model.optimize()
        sol = model.getBestSol()
        
        # Mostrar solucion
        solucion = [];
        for i in cantidadDeY:
            solucion.append(sol[y[i]]);

        #print("y*: "+str(solucion));

        return solucion;

# *********************************************************************************************************************************
