
from ModeloAuxPDC import *
from ModeloPrimalPDC import *
from ModeloDualPDC import *

import numpy as np
from time import time

# ****************************************************************************
# ******************************* Sub - Metodos ******************************
# ****************************************************************************

def seguirIterando(newPatron, ySolution):
    indices = range(len(newPatron));
    suma=0;
    for i in indices:
        suma = suma + newPatron[i]*ySolution[i];
    #print("Si es >1 seguimos, sino (<=1) paramos.")
    #print("valor: "+str(suma));
    if(suma>1):    
        #print("Seguimos generando patrones.");
        return True;
    else:
        #print("Paramos de generar.");
        return False;

def separador():
    print("*******************************************************************"+
    "*************************************************************************"+
    "******");

# ****************************************************************************
# ***************************** Metodo Principal *****************************
# ****************************************************************************

# Iniciamos el timer
start_time = time()

# Declaracion de inputs.
anchoRollo = 0;
cantidad = [];
ancho = [];

# Lectura de inputs.
f = open('input.txt','r')
message = f.read()                  ## Leemos el archivo
message = message.rsplit('\n')
contador=1;
for linea in message:
    if(contador==2):
        anchoRollo = int(linea[0:len(linea)]);
    if(contador>=4):
        indice_separator = linea.index(':');
        cant = linea[0:indice_separator];
        anch =  linea[indice_separator+1:len(linea)];
        cantidad.append(int(cant));
        ancho.append(int(anch));
    contador=contador+1;
f.close()

# Ordenamos los inputs (cantidad:ancho) en base a ancho.
tupla = list(zip(ancho, cantidad));
tuplaOrdenada = sorted(tupla, reverse=True);
ancho, cantidad = zip(*tuplaOrdenada)

indices = range(len(cantidad));
listIndices = [];
for i in indices:
    listIndices.append(i);

indiceCantidad = list(zip(listIndices, cantidad));
indiceAncho = list(zip(listIndices, ancho));

# ****************************************************************************

# Inicializamos el bucle.
listPatronesPrimal=[];
idPatron = 0;
listPatronesDual=[];
y = ModeloDualPDC.resolverDual(listPatronesDual, indiceCantidad);
ySolution = list(zip(listIndices, y))
newPatron = ModeloAuxPDC.resolverAuxiliar(indiceAncho, ySolution, anchoRollo);
newIndicePatron = list(zip(listIndices, newPatron))

# ****************************************************************************

#Bucle que se encarga de cargar los patrones
while seguirIterando(newPatron, y):
    #separador();
    newPatron.insert(0, idPatron);
    listPatronesPrimal.append(newPatron);
    listPatronesDual.append(newIndicePatron);
    y = ModeloDualPDC.resolverDual(listPatronesDual, indiceCantidad);
    ySolution = list(zip(listIndices, y))
    newPatron = ModeloAuxPDC.resolverAuxiliar(indiceAncho, ySolution, anchoRollo);
    newIndicePatron = list(zip(listIndices, newPatron))
    idPatron = idPatron + 1;

# ****************************************************************************

# Cuando ya no tenemos que iterar mas (Sum(u*.y*)<=1 no es un patron que mejora lo que tenemos) Resolvemos el primal.
#separador();
ModeloPrimalPDC.resolverPrimal(listPatronesPrimal,cantidad);

# Mostramos el tiempo de ejecucion.
separador();
execute_time = time() - start_time;
print("Tiempo de ejecucion transcurrido : %.10f segundos." % execute_time);

# ****************************************************************************
# ****************************************************************************