
# *********************************************************************************************************************************
# CONSIGNA
# Cuál es la mínima cantidad de rollos de 3 metros de ancho que necesitamos fabricar para satisfacer la siguiente demanda?
# 25 rollos de 250 cm de ancho.
# 97 rollos de 135 cm de ancho.
# 610 rollos de 108 cm de ancho.
# 395 rollos de 93 cm de ancho.
# 211 rollos de 42 cm de ancho.
# *********************************************************************************************************************************

from pyscipopt import Model, quicksum

class ModeloAuxPDC:
    
    @staticmethod
    def resolverAuxiliar(demandaBarras, ySolution, anchoRollo):
        
        # Modelo
        model = Model("TP2 : Patrones de corte - Auxiliar")  

        # Input
        cantidadDeU = range(len(demandaBarras));

        # Variables
        u = [model.addVar(vtype="INTEGER") for j in cantidadDeU]

        # Funcion objetivo
        model.setObjective(quicksum(ySol[1] * u[ySol[0]] for ySol in ySolution), sense="maximize");

        # Restricciones
        model.addCons(quicksum(dem[1] * u[dem[0]] for dem in demandaBarras) <= anchoRollo, "Restriccion.")

        # Optimizacion
        model.optimize()
        sol = model.getBestSol()

        # Mostrar solucion
        solucion = [];
        for i in cantidadDeU:
            solucion.append(int(sol[u[i]]));

        #print("NUEVO-POSIBLE-PATRON: "+str(solucion));
        return solucion;

# *********************************************************************************************************************************
