
# *****************************************************************************************************

# ModeloAuxPDC: "Algoritmo" que encuentra el nuevo patron.

* Input del Modelo Dual	
	ListaDePatrones
	CantidadDeBarra
* Input del Modelo Aux
	AnchosDeBarra	
	anchoRollo
	y*
* Input del Modelo Primal
	ListadePatrones
	CantidadDeBarra

# ALGORITMO

(1) . Resolvemos el Dual y obtenemos y*
(2) . Resolvemos el Aux y obtenemos el nuevo patron u*
(3) . Si Sum(u*.y*)<=1 no es un patron que mejora lo que tenemos y termina la iteracion y hacemos (5).
(4) . Si Sum(u*.y*)>1 se agrega a la lista de patrones y vuelve a (1).
(5) . Resolvemos el Primal.

# *****************************************************************************************************
